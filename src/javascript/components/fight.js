import { controls } from '../../constants/controls';
import { fightersDetails } from '../helpers/mockData';

const players = {
  FIRST: 0,
  SECOND: 1,
}

function compareArray(_array1, _array2) {
  let array1 = _array1.slice().sort();
  let array2 = _array2.slice().sort();
  return array1.length === array2.length && array1.every((value, index) => value === array2[index]);
}

class Player {

  __isBlocking = false;

  constructor(fighter) {
    this.fighter = fighter;
    this.keysPressed = [];
    this.health = fighter.health;
    this.lastCriticalTime = 0;

    this.canDealCritical = this.canDealCritical.bind(this);
    this.criticalDealed = this.criticalDealed.bind(this);
  }

  receiveDamage(damage) {
    this.health -= damage;
  }

  isAlive() {
    return this.health > 0.0 ? true : false;
  }

  handleKeyPress(keyCode) {
    this.keysPressed.push(keyCode);
    console.log(this.keysPressed);
    if (this.keysPressed.length >= 4)
      this.keysPressed = this.keysPressed.slice(1, 5);
  }

  canDealCritical(time, criticalComboKeysCombination) {
    console.log(this.keysPressed, criticalComboKeysCombination, compareArray(this.keysPressed, criticalComboKeysCombination))
    if (!compareArray(this.keysPressed, criticalComboKeysCombination))
      return false;
    if (Math.abs(time - this.lastCriticalTime) >= 10 * 1000) {
      return true;
    } else {
      return false;
    }
  }

  criticalDealed(time) {
    this.lastCriticalTime = time;
  }

  getHealthInPercent() {
    return Math.floor(this.health * 100 / this.fighter.health);
  }

  setBlocking(newBlocking) {
    this.__isBlocking = newBlocking;
  }

  isBlocking() {
    return this.__isBlocking;
  }

}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    let player1 = new Player(firstFighter);
    let player2 = new Player(secondFighter);

    const updateIndicators = updaterFighterIndicator();

    document.addEventListener('keyup', function(event) {
      if (event.code === controls.PlayerOneBlock) {
        player1.setBlocking(false);
      }
      if (event.code === controls.PlayerTwoBlock) {
        player2.setBlocking(false);
      }
    });

    document.addEventListener('keydown', function(event) {
      let damage = 0;
      switch (decidePlayerByButton(event.code)) {
        case players.FIRST: {
          player1.handleKeyPress(event.code);

          if (event.code === controls.PlayerOneBlock) {
            player1.setBlocking(true);
            break;
          }

          if (event.code === controls.PlayerOneAttack && player1.isBlocking() === false) 
            damage = getDamage(firstFighter, secondFighter);

          if (player2.isBlocking(controls.PlayerTwoBlock)) 
            damage = 0;

          const currentTime = Date.now();
          if (player1.canDealCritical(currentTime, controls.PlayerOneCriticalHitCombination)) {
            damage = getCriticalDamage(firstFighter);
            player1.criticalDealed(currentTime);
          }

          player2.receiveDamage(damage);
          updateIndicators(players.SECOND, player2.getHealthInPercent());
          break;
        }
        case players.SECOND: {
          player2.handleKeyPress(event.code);

          if (event.code === controls.PlayerTwoBlock) {
            player2.setBlocking(true);
            break;
          }

          if (event.code === controls.PlayerTwoAttack && player2.isBlocking() === false)
            damage = getDamage(secondFighter, firstFighter);

          if (player1.isBlocking(controls.PlayerOneBlock)) 
            damage = 0;

          const currentTime = Date.now();
          if (player2.canDealCritical(currentTime, controls.PlayerTwoCriticalHitCombination)) {
            damage = getCriticalDamage(firstFighter);
            player2.criticalDealed(currentTime);
          }

          player1.receiveDamage(damage);
          updateIndicators(players.FIRST, player1.getHealthInPercent());
          break;
        }
      }
    
      if (!player1.isAlive()) {
        resolve(secondFighter);
      }

      if (!player2.isAlive()) {
        resolve(firstFighter);
      }
    });
  });
}

function decidePlayerByButton(keyCode) {
  if (keyCode === controls.PlayerOneAttack || 
      keyCode === controls.PlayerOneBlock || 
      controls.PlayerOneCriticalHitCombination.includes(keyCode))
    return players.FIRST;
  if (keyCode === controls.PlayerTwoAttack || 
      keyCode === controls.PlayerTwoBlock || 
      controls.PlayerTwoCriticalHitCombination.includes(keyCode))
    return players.SECOND;
}

function updaterFighterIndicator() {

  let firstFighterIndicator = document.getElementById("left-fighter-indicator");
  let secondFighterIndicator = document.getElementById("right-fighter-indicator");
  console.log(firstFighterIndicator)

  return function updateIndicator(fighter, newHealth) {
    console.log(fighter, newHealth)
    switch (fighter) {
      case players.FIRST:
        firstFighterIndicator.style.width = `${newHealth}%`;
        break;
      case players.SECOND:
        secondFighterIndicator.style.width = `${newHealth}%`;
        break;
      default:
        console.error(`Wrong fighter: ${fighter}`);
        break;
    }
  }
}

export function getDamage(attacker, defender) {
  // return damage
  return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
}

function getCriticalDamage(fighter) {
  let power = fighter.attack * 2;
  return power;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  let power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = Math.random() + 1;
  let power = fighter.defense * dodgeChance;
  return power;
}