import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper'

export function showWinnerModal(fighter) {
  // call showModal function 
  let bodyElement = createElement({
    tagName: 'h1',
    attributes: '',
  })
  bodyElement.innerHTML = `<h1> ${fighter.name} win. Congrats!`
  const modalData = {
    title: `${fighter.name} win`,
    bodyElement,
    onClose: () => {location.reload();}
  }
  showModal(modalData);
}
