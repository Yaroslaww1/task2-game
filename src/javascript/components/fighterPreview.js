import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  try {
    const { attack,  defense, health, name } = fighter;
    fighterElement.append(createFighterImage(fighter));
    const fighterDescriptionElement = createElement({
      tagName: 'div',
      className: `fighter-preview___description-container ${positionClassName}`
    })
    fighterElement.append(fighterDescriptionElement);
    [
      {
        description: "attack",
        value: attack
      }, 
      {
        description: "defense",
        value: defense
      }, 
      {
        description: "health",
        value: health
      }, 
      {
        description: "name",
        value: name
      }
    ].forEach(element => {
      console.log(element);
      const descriptionElement = createElement({
        tagName: 'div',
        className: `fighter-preview___description ${positionClassName}`,
      });
      descriptionElement.innerText = `${element.description} : ${element.value}`;
      fighterDescriptionElement.append(descriptionElement);
    });
    // todo: show fighter info (image, name, health, etc.)

    return fighterElement;
  } catch (error) {
    return createElement({
      tagName: 'div',
      className: `${positionClassName}`
    });
  }
}

export function createFighterImage(fighter) {
  console.log("fighter: ", fighter);
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });

  return imgElement;
}
